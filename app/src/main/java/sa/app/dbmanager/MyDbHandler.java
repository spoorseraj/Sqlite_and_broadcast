package sa.app.dbmanager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDbHandler extends SQLiteOpenHelper {
    String TBLQuery = "" +
            "CREATE TABLE contacts(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT ," +
            "phoneNumber TEXT , " +
            "date Long " +
            ")";

    public MyDbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TBLQuery);
    }

    public void insert(String phoneNumbr) {
        Long date = System.currentTimeMillis();
        SQLiteDatabase db = this.getWritableDatabase();
        String q = "INSERT INTO contacts(phoneNumber,date)" +
                "VALUES('" + phoneNumbr + "','" + date + "')";
        db.execSQL(q);
        db.close();
    }
    public String getPhone(){
        StringBuilder result= new StringBuilder();
        SQLiteDatabase db=this.getReadableDatabase();
        String q="SELECT  phoneNumber,date FROM CONTACTS";
        Cursor cursor=db.rawQuery(q,null);
        while (cursor.moveToNext()){
            result.append(cursor.getString(0)).append(" ").append(cursor.getString(1)).append("\n");
        }
        return result.toString();

    }
    public void cleanData(){
        SQLiteDatabase db=this.getWritableDatabase();
        String q="DELETE  FROM CONTACTS";
        db.execSQL(q);
        db.close();
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
