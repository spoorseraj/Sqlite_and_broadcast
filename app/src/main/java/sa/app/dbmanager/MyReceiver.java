package sa.app.dbmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    Context pContext;
    MyDbHandler dbHandler;
    private static String mLastState;

    @Override
    public void onReceive(final Context context, Intent intent) {
        pContext = context;

        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (!state.equals(mLastState)) {
            mLastState = state;
        if (!intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {

            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            dbHandler = new MyDbHandler(pContext, "my_db.sqlite", null, 1);
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                Toast.makeText(context,  number, Toast.LENGTH_SHORT).show();
                dbHandler.insert(number);
                }
            }
        }
//        }


//        TelephonyManager telephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
//        telephony.listen(new PhoneStateListener(){
//            @Override
//            public void onCallStateChanged(int state, String incomingNumber) {
//                super.onCallStateChanged(state, incomingNumber);
//                Toast.makeText(pContext, incomingNumber, Toast.LENGTH_SHORT).show();
//                dbHandler=new MyDbHandler(pContext,"my_db.sqlite",null,1);
//                dbHandler.insert(incomingNumber);
//
//            }
//        },PhoneStateListener.LISTEN_CALL_STATE);


    }
}
